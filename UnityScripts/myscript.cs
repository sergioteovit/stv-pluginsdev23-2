using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myscript : MonoBehaviour
{
    public float mass = 0.1f;
    public Vector3 gravity = new Vector3(0.0f, -9.81f, 0.0f);
    private Vector3 x,x0;
    private Vector3 v;
    private Vector3 a;
    private Vector3 forces = new Vector3(0.0f, 0.0f, 0.0f);
    public float K = 0.5f;
    public float D = 0.0f;

    public GameObject particle;
    public GameObject reference;

    // Start is called before the first frame update
    void Start()
    {
        x0 = particle.transform.position;
        x = x0;
        particle.transform.position = x0;
        v = new Vector3(0.0f, 0.0f, 0.0f);
        a = new Vector3(0.0f, 0.0f, 0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        x0 = reference.transform.position;

        // suma de fuerzas
        forces = mass * gravity;
                 // - D * v
                 // - K * (x - x0); 

        // segunda ley de Newton F = m * a
        a = forces / mass;

        // Integramos la aceleración
        v += a * Time.deltaTime;

        // Integramos la velocidad
        x += v * Time.deltaTime;

        // Actualizamos la posición de la partícula
        particle.transform.position = x;// + reference.transform.position;

        // Dibujamos un enlace entre la partícula y su referencia
        Debug.DrawLine(x, x0);
    }
}
