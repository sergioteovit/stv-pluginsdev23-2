from maya import cmds

class CustomMenu:
  def __init__(self):
    self.win = cmds.window(title="Menu Example", menuBar = True, widthHeight =(300,200))
    fileMenu = cmds.menu(label ="File")
    loadOption = cmds.menuItem(label="Load")
    saveOption = cmds.menuItem(label="Save")
    cmds.setParent("..")
    
    objectsMenu = cmds.menu(label="Objects")
    sphereMI = cmds.menuItem(label="Make Sphere", command = self.sphereCommand)
    sphereOption = cmds.menuItem(optionBox = True,command = self.sphereCommandOptions)
    cubeMI = cmds.menuItem(label = "Make Cube",command = self.cubeCommand)
    cubeOption = cmds.menuItem(optionBox = True,command = self.cubeCommandOptions)
    cmds.setParent("..")
    
    cmds.columnLayout()
    cmds.text(label = "Put the rest of yout interface here")
    
    cmds.showWindow(self.win)
  
  def sphereCommand(self,*args):
    self.makeSphere(1)
  def sphereCommandOptions(self,*args):
    promptInput = cmds.promptDialog(title = "SphereRadius",message="Specify Radius: ", button=['OK','CANCEL'], defaultButton = 'OK',cancelButton = 'CANCEL',dismissString='CANCEL')
    
    if(promptInput == 'OK'):
      radiusInput = cmds.promptDialog(query=True,text=True)
      self.makeSphere(radiusInput)
  
  def cubeCommand(self,*args):
    self.makeCube(1)
  def cubeCommandOptions(self,*args):
    promptInput = cmds.promptDialog(title = "CubeSize",message="Specify Size: ", button=['OK','CANCEL'], defaultButton = 'OK',cancelButton = 'CANCEL',dismissString='CANCEL')
    
    if(promptInput == 'OK'):
      cubeInput = cmds.promptDialog(query=True,text=True)
      self.makeCube(cubeInput)
  
  def makeSphere(self,r):
    cmds.polySphere(radius = r)
  def makeCube(self,s):
    cmds.polyCube(w = s, h= s, d= s)
    
CustomMenu()