import maya.cmds as cmds
from math import *

cmds.delete( all=True, channels=True )

for theta in range(0,360,15):
    for phi in range(0,360,30):
        
        cmds.polySphere(name='body', axis=(0,1,0), radius=1)
        cmds.scale(1, 1, 1)
        cmds.rotate(0, 0, 0)
        cmds.move((5+1.25*cos(radians(phi)))*cos(radians(theta)), 
        (5+1.25*cos(radians(phi)))*sin(radians(theta)), 
        1.25*sin(radians(phi)))
        '''
        selectedObj = cmds.ls(selection=True)
        
        cmds.sets( name='redMaterialGroup'+phi+theta, renderable=True, empty=True )
        cmds.shadingNode( 'phong', name='redShader', asShader=True )
        cmds.setAttr( 'redShader.color', cos(radians(theta)), sin(radians(theta)), 0, type='double3' )
        cmds.surfaceShaderList( 'redShader', add='redMaterialGroup' )
        cmds.sets( selectedObj, e=True, forceElement='redMaterialGroup' )
        '''
        