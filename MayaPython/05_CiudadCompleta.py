import maya.cmds as cmds
import math
import random
from math import radians

cmds.file(new=True, f=True)

def buildTower(buildType, locx, locy, locz):
    setImportPath = ''
    
    if buildType == 1:
        setImportPath = 'D:/Buildings/Buildings01.obj'
    elif buildType == 2:
        setImportPath = 'D:/Buildings/Buildings02.obj'
    elif buildType == 3:
        setImportPath = 'D:/Buildings/Buildings03.obj'
    elif buildType == 4:
        setImportPath = 'D:/Buildings/Buildings04.obj'
    elif buildType == 5:
        setImportPath = 'D:/Buildings/Buildings05.obj'
    else:
        setImportPath = 'D:/Buildings/Buildings01.obj'
    
    before = set(cmds.ls(assemblies=True))
    model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
    after  = set(cmds.ls(assemblies=True))
    imported = after.difference(before)
    cmds.select(imported)
    cmds.rename('model')
    cmds.scale(0.1, 0.1, 0.1, pivot=(0, 0, 0), relative=True)
    cmds.move(locx, locy, locz)
    
def buildCity(num_builds):
    for coord_x in range(0,num_builds):
        for coord_y in range(0,num_builds):
            buildType = random.randint(1,5)
            myBuild = buildTower(buildType, 4*coord_x, 0 , 4*coord_y)
    
    
buildCity(5)