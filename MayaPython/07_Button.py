from maya import cmds

def buttonAction(args):
  print("Button is pressed")
  
def showUI():
  win = cmds.window(title="Button Event", widthHeight = (100,100))
  cmds.columnLayout()
  cmds.button(label="My Button", command = buttonAction)
  cmds.showWindow(win)
  
showUI()