import maya.cmds as cmds
import math
import random
from math import radians

def buildTower(buildType, locx, locy, locz):
    
    if buildType == 1:
        before = set(cmds.ls(assemblies=True))
        setImportPath = 'D:/untitled.obj'
        model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
        after  =set(cmds.ls(assemblies=True))
        imported = after.difference(before)
        cmds.select(imported)
        cmds.move(locx, locy, locz)
        
    elif buildType == 2:
        before = set(cmds.ls(assemblies=True))
        setImportPath = 'D:/cone.obj'
        model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
        after  =set(cmds.ls(assemblies=True))
        imported = after.difference(before)
        cmds.select(imported)
        cmds.move(locx, locy, locz)
        
    elif buildType == 3:
        before = set(cmds.ls(assemblies=True))
        setImportPath = 'D:/sphere.obj'
        model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
        after  =set(cmds.ls(assemblies=True))
        imported = after.difference(before)
        cmds.select(imported)
        cmds.move(locx, locy, locz)
        
    else:
        before = set(cmds.ls(assemblies=True))
        setImportPath = 'D:/cylinder.obj'
        model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
        after  =set(cmds.ls(assemblies=True))
        imported = after.difference(before)
        cmds.select(imported)
        cmds.move(locx, locy, locz)
        
        
    '''         
        
    elif buildType == 3:
       
        
        
    elif buildType == 4:
        
    elif buildType == 5:
        
        
    elif buildType == 6:
        
    elif buildType == 7:
        
    
    elif buildType == 8:
        
        
    elif buildType == 9:
        
        
    elif buildType == 10:
       
   
    else:
        
        
    '''
    

def buildCity(num_builds):
    for coord_x in range(0,num_builds):
        for coord_y in range(0,num_builds):
            buildType = random.randint(1,4)
            myBuild = buildTower(buildType, 3*coord_x, 1, 3*coord_y)
    
    
buildCity(5)