import maya.cmds as cmds

cmds.delete( all=True, channels=True )

# polyCone, polyCube, polyPlane, polySphere, polyTorus

def createSnowMan():
    cmds.polySphere(name='body', axis=(0,1,0), radius=1)
    cmds.scale(1.5, 1.5, 1.5)
    cmds.rotate(0, 0, 0)
    cmds.move(0, 0, 0)
    cmds.polySphere(name='head', axis=(0,1,0), radius=1)
    cmds.scale(1, 1, 1)
    cmds.rotate(0, 0, 0)
    cmds.move(0, 2, 0)
    cmds.polyCylinder(name='hat1', axis=(0,1,0), radius=1, height=0.3)
    cmds.scale(1, 1, 1)
    cmds.rotate(0, 0, 0)
    cmds.move(0, 3, 0)
    cmds.polyCylinder(name='hat2', axis=(0,1,0), radius=0.7, height=1)
    cmds.scale(1, 1, 1)
    cmds.rotate(0, 0, 0)
    cmds.move(0, 3.6, 0)
    cmds.polyCylinder(name='arms', axis=(1,0,0), radius=0.1, height=5)
    cmds.scale(1, 1, 1)
    cmds.rotate(0,90, 0)
    cmds.move(0, 0.5, 0)
    
createSnowMan()