import maya.cmds as cmds
import math
import random
from math import radians

cmds.file(new=True, f=True)

def buildTower(buildType, locx, locy, locz):
    setImportPath = ''
    
    if buildType == 1:
        setImportPath = 'D:/tree/arbol01.fbx'
    elif buildType == 2:
        setImportPath = 'D:/tree/arbol02.fbx'
    elif buildType == 3:
        setImportPath = 'D:/tree/arbol03.fbx'
    elif buildType == 4:
        setImportPath = 'D:/tree/arbol04.fbx'
    elif buildType == 5:
        setImportPath = 'D:/tree/arbol05.fbx'
    else:
        setImportPath = 'D:/tree/arbol06.fbx'
    
    before = set(cmds.ls(assemblies=True))
    model=cmds.file(setImportPath, i=True, mergeNamespacesOnClash=True, namespace=':')
    
    after  = set(cmds.ls(assemblies=True))
    imported = after.difference(before)
    cmds.select(imported)
    cmds.rename('model')
    cmds.move(locx, locy, locz)
    
def buildCity(num_builds):
    for coord_x in range(0,num_builds):
        for coord_y in range(0,num_builds):
            buildType = random.randint(1,5)
            myBuild = buildTower(buildType, 2*coord_x, 0 , 2*coord_y)
    
    
buildCity(5)