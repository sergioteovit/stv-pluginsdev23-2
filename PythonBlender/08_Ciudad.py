import bpy
import math
import random
from math import radians

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

def buildTower(buildType, locx, locy, locz):
    
    if buildType == 1:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
    elif buildType == 2:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.ops.object.join()
    elif buildType == 3:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube3'
        newobject.scale = (0.4,0.4,1)
        newobject.location = (locx,locy,6)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.data.objects['cube3'].select_set(True)
        bpy.ops.object.join()
    elif buildType == 4:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube3'
        newobject.scale = (0.4,0.4,1)
        newobject.location = (locx,locy,6)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube4'
        newobject.scale = (0.1,0.1,1)
        newobject.location = (locx,locy,8)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.data.objects['cube3'].select_set(True)
        bpy.data.objects['cube4'].select_set(True)
        bpy.ops.object.join()
    elif buildType == 5:
        file_loc = 'C:\\Users\\DAVINCI-ACER\\Desktop\\models\\build1.obj'
        bpy.ops.import_scene.obj(filepath=file_loc) 
        imported_object = bpy.context.selected_objects[0]
        imported_object.name = 'MyOBJModel'
        imported_object.scale = (1,1, 1)
        imported_object.location = (locx,locy,0)
        
        mat = bpy.data.materials.new(name="MaterialOBJModel")
        mat.diffuse_color = (random.randint(0,255)/255, random.randint(0,255)/255, random.randint(0,255)/255, 1.0)
        imported_object.data.materials[0] = mat
        
        mat.use_nodes = True
        bsdf = mat.node_tree.nodes["Principled BSDF"]
        texImage = mat.node_tree.nodes.new('ShaderNodeTexImage')
        texImage.image = bpy.data.images.load("C:\\Users\\DAVINCI-ACER\\Desktop\\models\\brick.jpg")
        mat.node_tree.links.new(bsdf.inputs['Base Color'], texImage.outputs['Color'])

        if imported_object.data.materials:
            imported_object.data.materials[0] = mat
        else:
            imported_object.data.materials.append(mat)
            
    elif buildType == 6:
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase1'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx,locy,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase1Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0)
        newobject.data.materials.append(mat)
        
        # third cube
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower1'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx,locy,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower1Name")
        mat.diffuse_color = (0.584, 0.035, 0.156, 1.0) 
        newobject.data.materials.append(mat) 
        
        
        #join objects
        bpy.data.objects['cubeBase1'].select_set(True)
        bpy.data.objects['flower1'].select_set(True)
        bpy.ops.object.join()
    else:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.scene.objects.active
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
    
    #create material
    scene = bpy.context.scene
    activeObject = bpy.context.active_object 
    activeObject.name = 'build'
    mat = bpy.data.materials.new(name="MaterialName") 
    activeObject.data.materials.append(mat) 
    red_component = random.randint(0,255)
    green_component = random.randint(0,255)
    blue_component = random.randint(0,255)
    bpy.context.object.active_material.diffuse_color = (red_component/255, green_component/255, blue_component/255,1)
    
    bpy.ops.mesh.primitive_plane_add(size=2, enter_editmode=False, location=(0, 0, 0))

    newobject = bpy.context.active_object
    newobject.name = 'ground'
    newobject.scale = (1.5,1.5,1.0)
    newobject.location = (locx,locy,0.0)
    matGround = bpy.data.materials.new(name="MaterialGroundName")
    matGround.diffuse_color = (0.5, 0.5, 0.5, 1.0) 
    newobject.data.materials.append(matGround) 
    
    bpy.data.objects['build'].select_set(True)
    bpy.data.objects['ground'].select_set(True)
    bpy.ops.object.join()
        
    return activeObject


def buildCity(num_builds):
    for coord_x in range(0,num_builds):
        for coord_y in range(0,num_builds):
            buildType = random.randint(1,6)
            myBuild = buildTower(buildType, 3*coord_x, 3*coord_y, 0)
    
    
buildCity(10)
    
