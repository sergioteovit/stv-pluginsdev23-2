import bpy
import math
from math import radians

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

theta = 0
while theta < 360:
    scene = bpy.context.scene
    bpy.ops.mesh.primitive_uv_sphere_add(segments=32, ring_count=16, 
radius=1.0, calc_uvs=True, enter_editmode=False, align='WORLD', 
location=(3.0*math.cos(radians(theta)), 3.0*math.sin(radians(theta)), 0), rotation=(0.0, 0.0, 0.0))
    activeObject = bpy.context.active_object 
    mat = bpy.data.materials.new(name="MaterialName") 
    activeObject.data.materials.append(mat) 
    bpy.context.object.active_material.diffuse_color = (math.cos(radians(theta)), math.sin(radians(theta)), 0,1) 
    theta+=20
