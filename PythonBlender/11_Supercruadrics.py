import bpy

from math import *

scene = bpy.context.scene

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

for theta in range(0,360,10):
        scene = bpy.context.scene
        bpy.ops.mesh.primitive_monkey_add(size=2.0, calc_uvs=True, enter_editmode=False, 
        align='WORLD', 
        location=(10.0*pow(cos(radians(theta)),3), 
        10.0*pow(sin(radians(theta)),3), 
        0.0), rotation=(0.0, 0.0, 0.0))
        activeObject = bpy.context.active_object 
        mat = bpy.data.materials.new(name="NewMaterial") 
        activeObject.data.materials.append(mat) 
        # Material in RGBA Format
        bpy.context.object.active_material.diffuse_color = (cos(radians(theta)),
        sin(radians(theta)), 0,0.5) 
        