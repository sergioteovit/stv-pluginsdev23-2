import bpy
import math

from math import radians

# comment

'''

Multiple line comment

'''

scene = bpy.context.scene

# 1) ciclo for 

for obj in scene.objects:
    
    # 2) Location
    
    obj.location.x += 10.0
    obj.location.y += 10.0
    obj.location.z += 10.0
    
    obj.location = (10,10,10)
    
    obj.location[0] += 1.0 # componente X
    obj.location[1] += 1.0
    obj.location[2] += 1.0
    
    # 3) Rotation
    
    #obj.rotation_euler.x += radians(0.0)
    #obj.rotation_euler.y += radians(0.0)
    #obj.rotation_euler.z += radians(45.0)
    
    # obj.rotation_euler = (radians(0), radians(270), radians(0))
    
    # obj.rotation_euler[0] += radians(60)
    # obj.rotation_euler[1] += radians(60)
    # obj.rotation_euler[2] += radians(0)
    
    # 4) Scale
    obj.scale.x += 0.1
    obj.scale.y += 2.0
    obj.scale.z += 0.1
    
    # obj.scale = (1,1,1)
    
    # obj.scale[0] += 2.0
    # obj.scale[1] += 2.0
    # obj.scale[2] += 2.0
    