import bpy
import math
import random
from math import *

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

# ponemos el cursor en el origen
bpy.context.scene.cursor.location[0] = 0
bpy.context.scene.cursor.location[1] = 0
bpy.context.scene.cursor.location[2] = 0



def fractalRight (value, height, px, py, pz, rx, ry, rz):
    if value < 0.01:
        return
    
    bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(0, 0, 0.3*height), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0, 0, 0), (0, 0, 0), (0, 0, 0)), "orient_matrix_type":'GLOBAL', "constraint_axis":(False, False, False), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":False, "use_accurate":False, "use_automerge_and_split":False})
    
    
    bpy.context.active_object.scale = (value, value, 1)
    bpy.context.active_object.rotation_euler[1] += radians(30)
    
    
    fractalRight( value*0.5, height, px, py, pz, rx, ry, rz)

def fractalLeft(value, height, px, py, pz, rx, ry, rz):
    if value < 0.01:
        return
        
    
    bpy.ops.object.duplicate_move(OBJECT_OT_duplicate={"linked":False, "mode":'TRANSLATION'}, TRANSFORM_OT_translate={"value":(0, 0, 0.3*height), "orient_axis_ortho":'X', "orient_type":'NORMAL', "orient_matrix":((0, 0, 0), (0, 0, 0), (0, 0, 0)), "orient_matrix_type":'GLOBAL', "constraint_axis":(False, False, False), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "view2d_edge_pan":False, "release_confirm":False, "use_accurate":False, "use_automerge_and_split":False})
    
    
    bpy.context.active_object.scale = (value, value, 1)
    bpy.context.active_object.rotation_euler[1] += radians(-30)
    
    
    fractalLeft( value*0.5, height, px, py, pz, rx, ry, rz)
    
        

height = 2
radius_c = 0.3
bpy.ops.mesh.primitive_cylinder_add(radius=radius_c, depth=height, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1), rotation=(0, 0, 0))
activeObject = bpy.context.active_object
        
activeObject.location[2] = 0.5*height

bpy.context.scene.cursor.location[0] = 0
bpy.context.scene.cursor.location[1] = 0
bpy.context.scene.cursor.location[2] = 0

bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
bpy.context.scene.transform_orientation_slots[0].type = 'NORMAL'

#bpy.ops.mesh.primitive_cylinder_add(radius=1, depth=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1), rotation=(radians(0), radians(0), radians(0)))

activeObject.name = 'base'
fractalRight(radius_c, height, 0,0,0, 0,0,0)

bpy.ops.object.select_all(action='DESELECT')
objectToSelect = bpy.data.objects["base"]
objectToSelect.select_set(True)    
bpy.context.view_layer.objects.active = objectToSelect
fractalLeft(radius_c, height, 0,0,0, 0,0,0)