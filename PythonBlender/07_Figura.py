import bpy
import math
from math import radians

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

def myCustomObject():
    # create object
    bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))

    #create material
    scene = bpy.context.scene
    activeObject = bpy.context.active_object 
    mat = bpy.data.materials.new(name="MaterialName") 
    activeObject.data.materials.append(mat) 
    bpy.context.object.active_material.diffuse_color = (math.cos(radians(theta)), math.sin(radians(theta)), 0,1) 
    
    return activeObject

theta = 0
while theta < 360:
    activeObject = myCustomObject()
    activeObject.location.x = 0.05*theta
    activeObject.location.y = 10.0*math.sin(radians(theta))
    activeObject.location.z = 2.0
    theta+=18
    
