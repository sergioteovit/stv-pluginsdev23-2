import bpy

def fractal(value, px, py, pz):
    
    if value < 0.01: 
        return
    
    rad_next = value/2
    bpy.ops.mesh.primitive_cylinder_add(radius=rad_next, depth=2, enter_editmode=False, align='WORLD', location=(px, py, pz + 2), scale=(1, 1, 1))
    fractal(rad_next, px, py, pz + 2)

base_size = 5
bpy.ops.mesh.primitive_cylinder_add(radius=base_size, depth=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
fractal(base_size, 0,0,0)