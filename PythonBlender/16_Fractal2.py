import bpy

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

def fractal(value, px, py, pz):
    if value < 0.05: 
        return
    
    obj_size = value/2
    
    bpy.ops.mesh.primitive_cube_add(size=value, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    activeObject = bpy.context.active_object
    activeObject.location = (px-3*obj_size,py,pz)
    fractal(obj_size, px-3 *obj_size,py,pz)
    
    bpy.ops.mesh.primitive_cube_add(size=value, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    activeObject = bpy.context.active_object
    activeObject.location = (px+3*obj_size,py,pz)
    fractal(obj_size, px+3*obj_size,py,pz)
    
    bpy.ops.mesh.primitive_cube_add(size=value, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    activeObject = bpy.context.active_object
    activeObject.location = (px,py+3*obj_size,pz)
    fractal(obj_size, px,py+3*obj_size,pz)
    
    bpy.ops.mesh.primitive_cube_add(size=value, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    activeObject = bpy.context.active_object
    activeObject.location = (px,py-3*obj_size,pz)
    fractal(obj_size, px,py-3*obj_size,pz)
    
    bpy.ops.mesh.primitive_cube_add(size=value, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    activeObject = bpy.context.active_object
    activeObject.location = (px,py,pz+3*obj_size)
    fractal(obj_size, px,py,pz+3*obj_size)
    
bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
fractal(1, 0,0,0)