import bpy
import math
import random
from math import radians

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

def buildElement(elemType, locx, locy, locz):
    if elemType == 1:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047, 1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (1,1,1)
        newobject.location = (locx,locy,3)
        
        mat = bpy.data.materials.new(name="MaterialHalfName")
        mat.diffuse_color = (0.058, 0.662, 0.094, 1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 2:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1.5,1.5, 0.1)
        newobject.location = (locx,locy,0.1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047, 1.0) 
        newobject.data.materials.append(mat) 
    elif elemType == 3:
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047, 1.0) 
        newobject.data.materials.append(mat) 
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        buildH1 = 1 + random.randint(1,2)
        newobject.scale = (1,1,1)
        newobject.location = (locx,locy,buildH1)
        
        mat = bpy.data.materials.new(name="MaterialHalfName")
        mat.diffuse_color = (0.478, 0.721, 0.039, 1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        buildH2 = buildH1 + random.randint(1,2)
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,buildH2)
        #join objects
        bpy.data.objects['cubeBase'].select_set(True)
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 4:
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase1'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx+0.5,locy+0.5,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase1Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0)
        newobject.data.materials.append(mat)
        
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase2'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx-0.5,locy-0.5,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase2Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0) 
        newobject.data.materials.append(mat)
        
        # third cube
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower1'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx+0.5,locy+0.5,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower1Name")
        mat.diffuse_color = (0.584, 0.035, 0.156, 1.0) 
        newobject.data.materials.append(mat) 
        
        # cube four
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower2'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx-0.5,locy-0.5,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower2Name")
        mat.diffuse_color = (0.584, 0.035, 0.156, 1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cubeBase1'].select_set(True)
        bpy.data.objects['cubeBase2'].select_set(True)
        bpy.data.objects['flower1'].select_set(True)
        bpy.data.objects['flower2'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 5:
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase1'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx+0.5,locy+0.5,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase1Name")
        mat.diffuse_color = (0.207, 0.584, 0.035,1.0)
        newobject.data.materials.append(mat)
        
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase2'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx-0.5,locy-0.5,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase2Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0) 
        newobject.data.materials.append(mat)
        
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase3'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx+0.5,locy-0.5,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase3Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0) 
        newobject.data.materials.append(mat)
        
        # 
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower1'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx+0.5,locy+0.5,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower1Name")
        red_component = random.randint(0,255)
        green_component = random.randint(0,255)
        blue_component = random.randint(0,255)
        mat.diffuse_color = (red_component/255, green_component/255, blue_component/255,1.0) 
        newobject.data.materials.append(mat) 
        
        # 
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower2'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx-0.5,locy-0.5,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower2Name")
        red_component = random.randint(0,255)
        green_component = random.randint(0,255)
        blue_component = random.randint(0,255)
        mat.diffuse_color = (red_component/255, green_component/255, blue_component/255,1.0) 
        newobject.data.materials.append(mat) 
        
        # 
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower3'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx+0.5,locy-0.5,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower3Name")
        red_component = random.randint(0,255)
        green_component = random.randint(0,255)
        blue_component = random.randint(0,255)
        mat.diffuse_color = (red_component/255, green_component/255, blue_component/255, 1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cubeBase1'].select_set(True)
        bpy.data.objects['cubeBase2'].select_set(True)
        bpy.data.objects['cubeBase3'].select_set(True)
        bpy.data.objects['flower1'].select_set(True)
        bpy.data.objects['flower2'].select_set(True)
        bpy.data.objects['flower3'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 6:
        # create object base
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cubeBase1'
        newobject.scale = (0.1,0.1, 0.5)
        newobject.location = (locx,locy,0.5)
        
        mat = bpy.data.materials.new(name="MaterialBase1Name")
        mat.diffuse_color = (0.207, 0.584, 0.035, 1.0)
        newobject.data.materials.append(mat)
        
        # third cube
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'flower1'
        newobject.scale = (0.25,0.25,0.25)
        newobject.location = (locx,locy,1.0)
        
        mat = bpy.data.materials.new(name="MaterialFlower1Name")
        mat.diffuse_color = (0.584, 0.035, 0.156, 1.0) 
        newobject.data.materials.append(mat) 
        
        
        #join objects
        bpy.data.objects['cubeBase1'].select_set(True)
        bpy.data.objects['flower1'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 7:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_cone_add(radius1=1, radius2=0, depth=2, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object 
        newobject.name = 'cone2'
        newobject.scale = (1,1,2)
        newobject.location = (locx,locy,4)
        
        mat = bpy.data.materials.new(name="MaterialHalfName")
        mat.diffuse_color = (0.584, 0.941, 0.039,1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cone2'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 8:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_cone_add(radius1=1, radius2=0, depth=2, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object 
        newobject.name = 'cone2'
        newobject.scale = (1.3,1.3,2)
        newobject.location = (locx,locy,4)
        
        mat = bpy.data.materials.new(name="MaterialHalf2Name")
        mat.diffuse_color = (0.086, 0.854, 0.086,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_cone_add(radius1=1, radius2=0, depth=2, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object 
        newobject.name = 'cone3'
        newobject.scale = (1,1,1)
        newobject.location = (locx,locy,5)
        
        mat = bpy.data.materials.new(name="MaterialHalf3Name")
        mat.diffuse_color = (0.086, 0.854, 0.086,1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cone2'].select_set(True)
        bpy.data.objects['cone3'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 9:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_ico_sphere_add(radius=1, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object 
        newobject.name = 'uvsphere2'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,3)
        
        mat = bpy.data.materials.new(name="MaterialHalfName")
        mat.diffuse_color = (0.058, 0.662, 0.094,1.0) 
        newobject.data.materials.append(mat) 
        
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['uvsphere2'].select_set(True)
        bpy.ops.object.join()
    elif elemType == 10:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2, 1)
        newobject.location = (locx,locy,1)
        
        mat = bpy.data.materials.new(name="MaterialBaseName")
        mat.diffuse_color = (0.333, 0.231, 0.047,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_ico_sphere_add(radius=1, enter_editmode=False, location=(0, 0, 0))

        newobject =bpy.context.active_object 
        newobject.name = 'uvsphere2'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,3)
        
        mat = bpy.data.materials.new(name="MaterialHalfName")
        mat.diffuse_color = (0.058, 0.662, 0.094,1.0) 
        newobject.data.materials.append(mat) 
        
        bpy.ops.mesh.primitive_ico_sphere_add(radius=1, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object 
        newobject.name = 'uvsphere3'
        newobject.scale = (2, 1.0, 1.0)
        newobject.location = (locx,locy,3)
        
        bpy.ops.mesh.primitive_ico_sphere_add(radius=1, enter_editmode=False, location=(0, 0, 0))

        newobject = bpy.context.active_object
        newobject.name = 'uvsphere4'
        newobject.scale = (1.0, 2.0, 1.0)
        newobject.location = (locx,locy,3)
        
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['uvsphere2'].select_set(True)
        bpy.data.objects['uvsphere3'].select_set(True)
        bpy.data.objects['uvsphere4'].select_set(True)
        bpy.ops.object.join()
    else:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
    
    #create material
    scene = bpy.context.scene
    activeObject = bpy.context.active_object
    activeObject.name = 'build'
    
    # bpy.ops.mesh.primitive_plane_add(size=2, enter_editmode=False, location=(0, 0, 0))
    bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))

    newobject = bpy.context.active_object
    newobject.name = 'ground'
    newobject.scale = (1.5,1.5,1.0)
    newobject.location = (locx,locy,-1.0)
    matGround = bpy.data.materials.new(name="MaterialGroundName")
    matGround.diffuse_color = (0.239, 0.435, 0.043, 1.0) 
    newobject.data.materials.append(matGround) 
    
    bpy.data.objects['build'].select_set(True)
    bpy.data.objects['ground'].select_set(True)
    bpy.ops.object.join()
    
    newCell = bpy.context.active_object
    newCell.name = "cell"
    newCell.location = (locx, locy, locz/4)
    
    return activeObject

def buildForest(num_X, num_Y):
    for coord_x in range(0,num_X):
        for coord_y in range(0,num_Y):
            #elemType = random.randint(1,10)
            height = 0
            
            if (coord_x >= 0) and (coord_x <= 6) and (coord_y >= 0) and (coord_y <= 3):
                elemType = random.randint(1,3)
                height = random.randint(0,1)
                
            if (coord_x >= 7) and (coord_x <= 9) and (coord_y >= 0) and (coord_y <= 3):
                elemType = random.randint(4,5)
                height = random.randint(2,4)
            
            if (coord_x >= 0) and (coord_x <= 4) and (coord_y >= 4) and (coord_y <= 9):
                elemType = random.randint(6,8)
                height = random.randint(0,1)
                
            if (coord_x >= 5) and (coord_x <= 9) and (coord_y >= 4) and (coord_y <= 9):
                elemType = random.randint(9,10)
            
            myElement = buildElement(elemType, 3*coord_x-3*(num_X-1)/2, 3*coord_y-3*(num_Y-1)/2, height)

buildForest(10, 10)
