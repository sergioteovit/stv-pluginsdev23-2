import bpy

def fractal(value, px, py, pz):
    
    if value < 0.005: 
        return
    
    rad_next = value/2
    bpy.ops.mesh.primitive_cube_add(size=rad_next, enter_editmode=False, align='WORLD', location=(px, py, pz+1.5*rad_next), scale=(1, 1, 1))
    fractal(rad_next, px, py, pz+1.5*rad_next)
    
    bpy.ops.mesh.primitive_cube_add(size=rad_next, enter_editmode=False, align='WORLD', location=(px+1.5*rad_next, py, pz), scale=(1, 1, 1))
    fractal(rad_next, px+1.5*rad_next, py, pz)
    
    bpy.ops.mesh.primitive_cube_add(size=rad_next, enter_editmode=False, align='WORLD', location=(px-1.5*rad_next, py, pz), scale=(1, 1, 1))
    fractal(rad_next, px-1.5*rad_next, py, pz)
    
    

base_size = 2
bpy.ops.mesh.primitive_cube_add(size=base_size, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
fractal(base_size, 0,0,0)

