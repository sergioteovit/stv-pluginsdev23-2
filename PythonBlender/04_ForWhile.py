import bpy

from math import *

scene = bpy.context.scene

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)


# 1) Ciclo while


'''
theta = 0

while theta < 360:
    
    bpy.ops.mesh.primitive_torus_add(align='WORLD', location=(10*cos(radians(theta)), 10*sin(radians(theta)), 20.0*sin(radians(theta))), 
rotation=(0.0, 0.0, 0.0), major_segments=48, minor_segments=12, 
mode='MAJOR_MINOR', major_radius=1.0, minor_radius=0.25, abso_major_rad=1.25, 
abso_minor_rad=0.75, generate_uvs=True)
    theta += 10
'''
'''
for i in range(0,20,1):  
    for j in range(0,20,1): 
        bpy.ops.mesh.primitive_plane_add(size=1.0, calc_uvs=True, enter_editmode=False, 
align='WORLD', location=(i, j, 0), rotation=(0.0, 0.0, 0.0)) 
'''


# 2) ciclo for

for theta in range(0,360,10):
    
    bpy.ops.mesh.primitive_monkey_add(size=2.0, calc_uvs=True, enter_editmode=False, 
align='WORLD', location=(10*sin(radians(theta)), 10*cos(radians(theta)), 0.0), rotation=(0.0, 0.0, 0.0))


'''
# 3) doble ciclo for

for theta in range(0,180,10):
    for phi in range(0,360,10):
        
        #bpy.ops.mesh.primitive_monkey_add(size=2.0, calc_uvs=True, enter_editmode=False, 
#align='WORLD', location=(10*sin(radians(theta))*sin(radians(phi)), 10*cos(radians(theta)), 10*sin(radians(theta))*cos(radians(phi))), rotation=(0.0, 0.0, 0.0))
        bpy.ops.object.metaball_add(type='CUBE', radius=1, enter_editmode=False, location=(10*sin(radians(theta))*sin(radians(phi)), 10*cos(radians(theta)), 10*sin(radians(theta))*cos(radians(phi))))
'''