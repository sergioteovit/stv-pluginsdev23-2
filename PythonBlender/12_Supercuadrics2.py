import bpy

from math import *

scene = bpy.context.scene

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

for theta in range(0,180,10):
    for phi in range(0,360,10):
        scene = bpy.context.scene
        bpy.ops.mesh.primitive_monkey_add(size=2.0, 
        calc_uvs=True, enter_editmode=False, 
        align='WORLD', 
        location=(10*pow(cos(radians(phi)),3)*pow(cos(radians(theta)),1), 
        10*pow(sin(radians(phi)),3), 
        10*pow(cos(radians(phi)),3)*pow(sin(radians(theta)),1)), rotation=(0.0, 0.0, 0.0))