import bpy
import math
import random
from math import radians

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

def buildTower(buildType, locx, locy, locz):
    
    if buildType == 1:
        # create object
        # bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        bpy.ops.mesh.primitive_cylinder_add(radius=1, depth=2, enter_editmode=False, location=(0, 0, 1))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (0.2,0.2,1.5)
        newobject.location = (locx,locy,1.5)
        
        bpy.ops.mesh.primitive_cone_add(radius1=1, radius2=0, depth=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cone'
        newobject.scale = (1,1,1)
        newobject.location = (locx,locy,3)
        
        bpy.ops.mesh.primitive_cone_add(radius1=1, radius2=0, depth=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cone'
        newobject.scale = (1,1,1)
        newobject.location = (locx,locy,4)
        
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cone'].select_set(True)
        bpy.ops.object.join()

    elif buildType == 2:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.ops.object.join()
    elif buildType == 3:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube3'
        newobject.scale = (0.4,0.4,1)
        newobject.location = (locx,locy,6)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.data.objects['cube3'].select_set(True)
        bpy.ops.object.join()
    elif buildType == 4:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube2'
        newobject.scale = (0.7,0.7,1)
        newobject.location = (locx,locy,4)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube3'
        newobject.scale = (0.4,0.4,1)
        newobject.location = (locx,locy,6)
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.active_object 
        newobject.name = 'cube4'
        newobject.scale = (0.1,0.1,1)
        newobject.location = (locx,locy,8)
        #join objects
        bpy.data.objects['cube1'].select_set(True)
        bpy.data.objects['cube2'].select_set(True)
        bpy.data.objects['cube3'].select_set(True)
        bpy.data.objects['cube4'].select_set(True)
        bpy.ops.object.join()
    else:
        # create object
        bpy.ops.mesh.primitive_cube_add(size=2, enter_editmode=False, location=(0, 0, 0))
        newobject = bpy.context.scene.objects.active
        newobject.name = 'cube1'
        newobject.scale = (1,1,1.5)
        newobject.location = (locx,locy,1.5)
    
    #create material
    scene = bpy.context.scene
    activeObject = bpy.context.active_object 
    activeObject.name = 'build'
    mat = bpy.data.materials.new(name="MaterialName") 
    activeObject.data.materials.append(mat) 
    red_component = random.randint(0,255)
    green_component = random.randint(0,255)
    blue_component = random.randint(0,255)
    bpy.context.object.active_material.diffuse_color = (red_component/255, green_component/255, blue_component/255,1)
        
    return activeObject


def buildCity(num_builds):
    for coord_x in range(0,num_builds):
        for coord_y in range(0,num_builds):
            buildType = random.randint(1,4)
            bpy.ops.mesh.primitive_plane_add(size=3, enter_editmode=False, location=(3*coord_x, 3*coord_y, 0))

            myBuild = buildTower(buildType, 3*coord_x, 3*coord_y, 0)
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.join()


buildCity(10)


