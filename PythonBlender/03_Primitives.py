import bpy

from math import *

scene = bpy.context.scene

bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False, confirm=False)

# 1) Primitives

bpy.ops.mesh.primitive_uv_sphere_add(segments=32, ring_count=16, 
radius=1.0, calc_uvs=True, enter_editmode=False, align='WORLD', 
location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))

bpy.ops.mesh.primitive_torus_add(align='WORLD', location=(0.0, 0.0, 0.0), 
rotation=(0.0, 0.0, 0.0), major_segments=48, minor_segments=12, 
mode='MAJOR_MINOR', major_radius=1.0, minor_radius=0.25, abso_major_rad=1.25, 
abso_minor_rad=0.75, generate_uvs=True)

bpy.ops.mesh.primitive_plane_add(size=2.0, calc_uvs=True, enter_editmode=False, 
align='WORLD', location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))

bpy.ops.mesh.primitive_monkey_add(size=2.0, calc_uvs=True, enter_editmode=False, 
align='WORLD', location=(0.0, 0.0, 0.0), rotation=(0.0, 0.0, 0.0))

bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=2, radius=1.0, 
calc_uvs=True, enter_editmode=False, align='WORLD', location=(0.0, 0.0, 0.0), 
rotation=(0.0, 0.0, 0.0))



'''
Blender Operators
https://docs.blender.org/api/current/bpy.ops.html#

Mesh operators
https://docs.blender.org/api/current/bpy.ops.mesh.html

Object operators
https://docs.blender.org/api/current/bpy.ops.object.html
'''