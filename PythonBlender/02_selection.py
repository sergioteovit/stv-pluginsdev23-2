import bpy

from math import *

scene = bpy.context.scene

# 1) object transformation by selection

# active_object = bpy.context.active_object 

# active_object.location = (10,10,10)
# active_object.rotation_euler = (radians(0),radians(0),radians(45))
# active_object.scale = (5,5,5)

# 2) object transformation by name

# my_object = scene.objects['Cube']

# my_object.location = (5,5,5)
# my_object.rotation_euler = (radians(0), radians(0), radians(90))
# my_object.scale = (2,2,2)

# 3) rename object

# my_object.name = 'MyNewCube'

# my_object = scene.objects['MyNewCube']

# my_object.location = (0,0,0)
# my_object.rotation_euler = (radians(0), radians(0), radians(90))
# my_object.scale = (5,6,5)