bl_info = {
    "name": "Mi Panel",
    "author": "Serch",
    "version": (0, 0, 1),
    "blender": (2, 91, 0),
    "location": "View3D > Add > Curve",
    "description": "Adds a new panel",
    "warning": "",
    "category": "Add Figures",
}


import bpy

class MyGeometriesPanel(bpy.types.Panel):
    """Creates a Panel in the Object properties window"""
    bl_label = "Serch Panel"
    bl_idname = "OBJECT_PT_panel"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"

    def draw(self, context):
        layout = self.layout

        obj = context.object

        row = layout.row()
        row.label(text="Hola Sergio!", icon='WORLD_DATA')

        row = layout.row()
        row.label(text="El objeto activo es: " + obj.name)
        row = layout.row()
        row.prop(obj, "name")
        row.prop(obj, "name")
        
        row = layout.row()
        row.operator("mesh.primitive_cube_add")
        row = layout.row()
        row.operator("mesh.primitive_ico_sphere_add")
        row = layout.row()
        row.operator("mesh.primitive_circle_add")
        row = layout.row()
        row.operator("mesh.primitive_cone_add")
        row = layout.row()
        row.operator("mesh.primitive_cylinder_add")
        row = layout.row()
        row.operator("mesh.primitive_grid_add")
        row = layout.row()
        row.operator("mesh.primitive_monkey_add")
        row = layout.row()
        row.operator("mesh.primitive_plane_add")
        row = layout.row()
        row.operator("mesh.primitive_torus_add")
        row = layout.row()
        row.operator("mesh.primitive_uv_sphere_add")
        row = layout.row()
        row.operator("object.select_all").action = 'TOGGLE'
        colmn = layout.column()
        colmn.operator("mesh.primitive_uv_sphere_add")
        colmn.operator("mesh.primitive_uv_sphere_add")
        colmn.operator("object.simple_operator")

class SimpleOperator(bpy.types.Operator):
    bl_idname = "object.simple_operator"
    bl_label = "My action"

    def execute(self, context):
        print("Hello world!")
        return {'FINISHED'}

def register():
    bpy.utils.register_class(MyGeometriesPanel)
    bpy.utils.register_class(SimpleOperator)


def unregister():
    bpy.utils.unregister_class(MyGeometriesPanel)
    bpy.utils.unregister_class(SimpleOperator)


if __name__ == "__main__":
    register()