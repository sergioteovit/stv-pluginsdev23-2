
#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include "geometry.h"
#include "cube.h"
#include "sphere.h"

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

class Cube;
class Sphere;
class Geometry;

class MainWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit MainWidget(QWidget *parent = 0);
    ~MainWidget();

    void changeTexture(QString filename);

protected:
    void mousePressEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *e) Q_DECL_OVERRIDE;
    void timerEvent(QTimerEvent *e) Q_DECL_OVERRIDE;

    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int w, int h) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void initShaders();
    void initTextures(QString filename);

public slots:
    void updatePosition(double px, double py, double pz);
    void updateRotation(double rx, double ry, double rz);
    void updateScale(double sx, double sy, double sz);

private:
    QBasicTimer          timer;
    QOpenGLShaderProgram program;

    Geometry             *geom;
    Geometry             *geom_sph;

    QOpenGLTexture       *texture;

    QMatrix4x4           projection;

    QVector2D            mousePressPosition;
    QVector3D            rotationAxis;
    qreal                angularSpeed;
    QQuaternion          rotation;

    // Transformations
    double             px, py, pz; // position
    double             rx, ry, rz; // rotation
    double             sx, sy, sz; // scale
};

#endif // MAINWIDGET_H
