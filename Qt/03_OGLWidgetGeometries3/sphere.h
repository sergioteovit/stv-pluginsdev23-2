#ifndef SPHERE_H
#define SPHERE_H

#include "vertexdata.h"

class Sphere
{
public:
    Sphere();

    VertexData *buildVertices(uint &nv);
    uint       *buildIndices (uint &ni);

protected:
    int vertical_degrees;   // vertical degrees
    int horizontal_degrees; // horizontal degrees
    int space;
};

#endif // SPHERE_H
