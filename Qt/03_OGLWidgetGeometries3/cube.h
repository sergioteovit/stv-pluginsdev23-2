
#ifndef Cube_H
#define Cube_H

#include "vertexdata.h"

class Cube
{
public:
    Cube();

    VertexData *buildVertices(uint &nv);
    uint       *buildIndices (uint &ni);

};

#endif // Cube_H
