#include "MainWindow.h"
#include "ui_MainWindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMenu *menuTextures = ui->menuBar->addMenu(tr("&Texturas"));
    menuTextures->addSeparator()->setText(tr("Elegir textura"));
    texturesGroup = new QActionGroup(this);

    QDir texturesDirectory("./textures");
    QStringList filters;
    filters << "*.png" << "*.jpg";
    texturesDirectory.setNameFilters(filters);
    QStringList fileNames = texturesDirectory.entryList();

    for (int i = 0; i < fileNames.size(); i++){
        QAction *addNewAction = new QAction(menuTextures);
        addNewAction->setText( fileNames.at(i) );
        texturesGroup->addAction( addNewAction );
        menuTextures->addAction( addNewAction );
        addNewAction->setCheckable(true);
        addNewAction->setChecked(true);
        connect(addNewAction, &QAction::triggered, this, &MainWindow::changeTexture);
    }

    menuTextures->addSeparator();

    // Connect transformations functions
    connect(ui->PosX, SIGNAL(valueChanged(double)), this, SLOT(updatePosition(double)) );

    // 1. Agregar funcionalidad para todos los controles de transpformación de posición
    // 2. Agregar funcionalidad para todos los controles de transpformación de rotación
    // 3. Agregar funcionalidad para todos los controles de transpformación de escala

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeTexture()
{
    qDebug() << texturesGroup->checkedAction()->text();
    MainWidget *glWidgetManage = ui->glWidget;
    glWidgetManage->changeTexture( tr("textures/") + texturesGroup->checkedAction()->text() );
}

void MainWindow::updatePosition(double)
{
    MainWidget *glWidgetManage = ui->glWidget;
    glWidgetManage->updatePosition(ui->PosX->value(),
                                   ui->PosY->value(),
                                   ui->PosZ->value());
}

void MainWindow::updateRotation(double)
{
    MainWidget *glWidgetManage = ui->glWidget;
    glWidgetManage->updateRotation(ui->RotX->value(),
                                   ui->RotY->value(),
                                   ui->RotZ->value());
}

void MainWindow::updateScale(double)
{
    MainWidget *glWidgetManage = ui->glWidget;
    glWidgetManage->updateScale(ui->ScaleX->value(),
                                ui->ScaleY->value(),
                                ui->ScaleZ->value());
}
