#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QDir>
#include <QActionGroup>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void changeTexture();
    void updatePosition(double);
    void updateRotation(double);
    void updateScale(double);

private:
    Ui::MainWindow *ui;

    // texture selector
    QActionGroup *texturesGroup;
};

#endif // MAINWINDOW_H
