#-------------------------------------------------
#
# Project created by QtCreator 2017-01-18T13:18:21
#
#-------------------------------------------------

QT       += core gui openglwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OGLWidgetGeometries3
TEMPLATE = app


SOURCES += main.cpp\
           MainWindow.cpp \
           mainwidget.cpp \
    geometry.cpp \
    cube.cpp \
    sphere.cpp

HEADERS  += MainWindow.h \
            mainwidget.h \
    vertexdata.h \
    geometry.h \
    cube.h \
    sphere.h


FORMS    += MainWindow.ui

RESOURCES += \
    shaders.qrc \
    textures.qrc
