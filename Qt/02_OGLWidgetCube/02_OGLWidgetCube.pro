#-------------------------------------------------
#
# Project created by QtCreator 2017-01-18T13:18:21
#
#-------------------------------------------------

QT       += core gui openglwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OGLWidgetCube
TEMPLATE = app


SOURCES += main.cpp\
           MainWindow.cpp \
           mainwidget.cpp \
           geometryengine.cpp

HEADERS  += MainWindow.h \
            mainwidget.h \
            geometryengine.h


FORMS    += MainWindow.ui

RESOURCES += \
    shaders.qrc \
    textures.qrc
