#include "MainWindow.h"
#include "ui_MainWindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMenu *menuTextures = ui->menuBar->addMenu(tr("&Textures"));
    menuTextures->addSeparator()->setText(tr("Choose texture"));
    texturesGroup = new QActionGroup(this);

    QDir texturesDirectory("./textures");
    QStringList filters;
    filters << "*.png" << "*.jpg";
    texturesDirectory.setNameFilters(filters);
    QStringList fileNames = texturesDirectory.entryList();

    for (int i = 0; i < fileNames.size(); i++){
        QAction *addNewAction = new QAction(menuTextures);
        addNewAction->setText( fileNames.at(i) );
        texturesGroup->addAction( addNewAction );
        menuTextures->addAction( addNewAction );
        addNewAction->setCheckable(true);
        addNewAction->setChecked(true);
        connect(addNewAction, &QAction::triggered, this, &MainWindow::changeTexture);
    }

    menuTextures->addSeparator();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeTexture()
{
    qDebug() << texturesGroup->checkedAction()->text();
    MainWidget *glWidgetManage = ui->glWidget;
    glWidgetManage->changeTexture( tr("textures/") + texturesGroup->checkedAction()->text() );
}
