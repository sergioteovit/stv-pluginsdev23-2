TEMPLATE      = lib
CONFIG       += plugin static
QT           += widgets
INCLUDEPATH  += ../../app-host
HEADERS += \
    cone.h \
    cube.h \
    sphere.h \
    cylinder.h

SOURCES += \
    cone.cpp \
    cube.cpp \
    sphere.cpp \
    cylinder.cpp

TARGET        = staticplugins
if(!debug_and_release|build_pass):CONFIG(debug, debug|release) {
   mac:TARGET = staticplugins_debug
   win32:TARGET = staticpluginsd
}

DESTDIR       = ../../plugins

DISTFILES +=


