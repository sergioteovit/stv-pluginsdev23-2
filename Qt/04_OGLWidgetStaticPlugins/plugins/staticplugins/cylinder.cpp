#include "cylinder.h"

///////////////////////////////////////////////////

#include <QtMath>

uint *CylinderPlugin::buildIndices(uint &ni)
{
    ni   = 0;
    return 0;
}

VertexData *CylinderPlugin::buildVertices(uint &nv)
{

    int vertical_degrees = 180;   // vertical degrees
    int horizontal_degrees = 360; // horizontal degrees
    int space = 10;

    int numParalels = vertical_degrees/space + 1;
    int numMeridians = horizontal_degrees/space;

    int    VertexCount = numParalels * numMeridians * 4;

    VertexData *vertices = new VertexData[VertexCount];

    int n = 0;
    int phi = 0;
    double p_radians;

    for ( phi = 0; phi <= horizontal_degrees; phi+=space ){
        for ( int yCoord = 0; yCoord < 3; yCoord++ ){
            p_radians = qDegreesToRadians((double)phi);

            // pivot points 1
            vertices[n] = {QVector3D(qSin( p_radians ) ,
                                     yCoord ,
                                     qCos( p_radians )
                                    )
                           ,
                           QVector2D( (double)phi/horizontal_degrees, (double)(yCoord)/3)};
            n++;

            // pivot points 3
            p_radians = qDegreesToRadians((double)phi+space);

            vertices[n] = {QVector3D(qSin( p_radians ) ,
                                     yCoord,
                                     qCos( p_radians )
                                    )
                           ,
                           QVector2D( (double)(phi+space)/horizontal_degrees, (double)(yCoord)/3)};
            n++;

            // pivot points 2
            p_radians = qDegreesToRadians( (double)(phi) );

            vertices[n] = {QVector3D(qSin( p_radians ) ,
                                     yCoord+1,
                                     qCos( p_radians )
                                    )
                           ,
                           QVector2D( (double)(phi)/horizontal_degrees, (double)(yCoord+1)/3)};
            n++;



            // pivot points 4
            p_radians = qDegreesToRadians( (double)(phi+space) );

            vertices[n] = {QVector3D(qSin( p_radians ) ,
                                     yCoord+1,
                                     qCos( p_radians )
                                    )
                           ,
                           QVector2D( (double)(phi+space)/horizontal_degrees, (double)(yCoord+1)/3)};
            n++;
        }
    }

    nv = n;
    return vertices;
}

