#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QDir>
#include <QActionGroup>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void changeTexture();
    void insertShape();

private:
    void loadTextures();
    void loadPlugins();
    void addToMenu(QObject *plugin, const QStringList &texts, QMenu *menu,
                   const char *member, QActionGroup *actionGroup = 0);

protected:
    Ui::MainWindow *ui;

    // texture selector
    QActionGroup *texturesGroup;
    QActionGroup *shapesGroup;
};

#endif // MAINWINDOW_H
