#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QPluginLoader>
#include <interfaces.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Configure textures menu
    loadTextures();

    // Load static plugins
    loadPlugins();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeTexture()
{
    MainWidget *glWidgetManage = ui->glWidget;

    if (!glWidgetManage) return;

    glWidgetManage->changeTexture( tr("textures/") + texturesGroup->checkedAction()->text() );
}

void MainWindow::loadTextures()
{
    // Menú de texturas
    QMenu *menuTextures = ui->menuBar->addMenu(tr("&Texturas"));
    menuTextures->addSeparator()->setText(tr("Elegir textura"));
    texturesGroup = new QActionGroup(this);

    QDir texturesDirectory("./textures");
    QStringList filters;
    filters << "*.png" << "*.jpg";
    texturesDirectory.setNameFilters(filters);
    QStringList fileNames = texturesDirectory.entryList();

    for (int i = 0; i < fileNames.size(); i++){
        QAction *addNewAction = new QAction(menuTextures);
        addNewAction->setText( fileNames.at(i) );
        texturesGroup->addAction( addNewAction );
        menuTextures->addAction( addNewAction );
        addNewAction->setCheckable(true);
        addNewAction->setChecked(true);
        connect(addNewAction, &QAction::triggered, this, &MainWindow::changeTexture);
    }

    menuTextures->addSeparator();
}

void MainWindow::loadPlugins()
{
    // Menú de formas
    QMenu *menuShapes = ui->menuBar->addMenu(tr("&Formas"));
    menuShapes->addSeparator()->setText(tr("Elegir forma"));
    shapesGroup = new QActionGroup(this);
    // Load static plugins
    foreach (QObject *plugin, QPluginLoader::staticInstances()){
        ShapeInterface *iShape = qobject_cast<ShapeInterface *>(plugin);
        if (iShape){
            addToMenu(plugin, iShape->info(), menuShapes, SLOT(insertShape()), shapesGroup);
        }
    }

    menuShapes->addSeparator();
}

void MainWindow::addToMenu(QObject *plugin, const QStringList &texts,
                           QMenu *menu, const char *member,
                           QActionGroup *actionGroup)
{
    foreach (QString text, texts) {
        QAction *action = new QAction(text, plugin);
        connect(action, SIGNAL(triggered()), this, member);
        menu->addAction(action);

        if (actionGroup) {
            action->setCheckable(true);
            actionGroup->addAction(action);
        }
    }
}

void MainWindow::insertShape()
{
    if (!shapesGroup) return;

    QAction *selectedAction = shapesGroup->checkedAction();

    if ( !selectedAction ) return;

    ShapeInterface *iShape = qobject_cast<ShapeInterface *>(selectedAction->parent());

    if ( !iShape ) return;

    MainWidget *glWidgetManage = ui->glWidget;

    if (!glWidgetManage) return;

    Geometry  *g = new Geometry;
    uint nv = 0, ni = 0;
    VertexData *data = iShape->buildVertices(nv);
    uint       *indices = iShape->buildIndices(ni);
    g->build(data, nv, indices, ni);
    glWidgetManage->changeShape( g );
}

