#ifndef RelojPLUGIN_H
#define RelojPLUGIN_H

#include "interfaces.h"
#include "vertexdata.h"

#include <QObject>
#include <QtPlugin>

class RelojPlugin : public QObject,
                   public ShapeInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID "app-host.OGLWidget.ShapeInterface" FILE "reloj.json")

    Q_INTERFACES(ShapeInterface)

public:

    VertexData *buildVertices(uint &nv);
    uint       *buildIndices (uint &ni);

    QStringList info() { return QStringList() << tr("MI Reloj"); }

};

#endif // RelojPLUGIN_H
