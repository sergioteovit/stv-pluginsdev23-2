TEMPLATE      = lib
CONFIG       += plugin
QT           += widgets
INCLUDEPATH  += ../../app-host

TARGET        = reloj
DESTDIR       = ../../plugins

HEADERS += \
    reloj.h

SOURCES += \
    reloj.cpp
