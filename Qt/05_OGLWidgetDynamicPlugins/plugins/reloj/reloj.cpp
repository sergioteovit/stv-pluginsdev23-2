#include "reloj.h"

///////////////////////////////////////////////////

#include <QtMath>

uint *RelojPlugin::buildIndices(uint &ni)
{
    ni   = 0;
    return 0;
}

VertexData *RelojPlugin::buildVertices(uint &nv)
{
        int vertical_degrees = 180;   // vertical degrees
        int horizontal_degrees = 360; // horizontal degrees
        int space = 10;

        int numParalels = vertical_degrees/space + 1;
        int numMeridians = horizontal_degrees/space;

        int    VertexCount = numParalels * numMeridians * 8;

        VertexData *vertices = new VertexData[VertexCount];

        int n = 0;
        int phi = 0;
        double p_radians;

        for ( phi = 0; phi <= horizontal_degrees; phi+=space ){
            //for ( int yCoord = 0; yCoord < 2; yCoord++ ){
                p_radians = qDegreesToRadians((double)phi);

                // pivot points 1
                vertices[n] = {QVector3D(qSin( p_radians ) ,
                                         -1.0f,
                                         qCos( p_radians )
                                        )
                               ,
                               QVector2D( (double)phi/horizontal_degrees, 0)};
                n++;

                // pivot points 3
                p_radians = qDegreesToRadians((double)phi+space);

                vertices[n] = {QVector3D(qSin( p_radians ) ,
                                         -1.0,
                                         qCos( p_radians )
                                        )
                               ,
                               QVector2D( (double)(phi+space)/horizontal_degrees, 0)};
                n++;

                // pivot points 2
                p_radians = qDegreesToRadians( (double)(phi) );

                vertices[n] = {QVector3D(0 ,
                                         0,
                                         0
                                        )
                               ,
                               QVector2D( (double)(phi)/horizontal_degrees, 1)};
                n++;



                // pivot points 4
                p_radians = qDegreesToRadians( (double)(phi+space) );

                vertices[n] = {QVector3D(0 ,
                                         0,
                                         0
                                        )
                               ,
                               QVector2D( (double)(phi+space)/horizontal_degrees, 1)};
                n++;
            //}
        }

        for ( phi = 0; phi <= horizontal_degrees; phi+=space ){
            //for ( int yCoord = 0; yCoord < 2; yCoord++ ){
                p_radians = qDegreesToRadians((double)phi);

                // pivot points 1
                vertices[n] = {QVector3D(qSin( p_radians ) ,
                                         1.0f,
                                         qCos( p_radians )
                                        )
                               ,
                               QVector2D( (double)phi/horizontal_degrees, 0)};
                n++;

                // pivot points 3
                p_radians = qDegreesToRadians((double)phi+space);

                vertices[n] = {QVector3D(qSin( p_radians ) ,
                                         1.0,
                                         qCos( p_radians )
                                        )
                               ,
                               QVector2D( (double)(phi+space)/horizontal_degrees, 0)};
                n++;

                // pivot points 2
                p_radians = qDegreesToRadians( (double)(phi) );

                vertices[n] = {QVector3D(0 ,
                                         0,
                                         0
                                        )
                               ,
                               QVector2D( (double)(phi)/horizontal_degrees, 1)};
                n++;



                // pivot points 4
                p_radians = qDegreesToRadians( (double)(phi+space) );

                vertices[n] = {QVector3D(0 ,
                                         0,
                                         0
                                        )
                               ,
                               QVector2D( (double)(phi+space)/horizontal_degrees, 1)};
                n++;
            //}
        }

        nv = n;
        return vertices;
}

