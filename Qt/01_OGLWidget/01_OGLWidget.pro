#-------------------------------------------------
#
# Project created by QtCreator 2017-01-18T13:18:21
#
#-------------------------------------------------

QT       += core gui openglwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OGLWidget
TEMPLATE = app


SOURCES += main.cpp\
           MainWindow.cpp \
           glwidget.cpp \
           glwidget2.cpp \
           qtlogo.cpp

HEADERS  += MainWindow.h \
            glwidget.h \
            glwidget2.h \
            qtlogo.h

FORMS    += MainWindow.ui
