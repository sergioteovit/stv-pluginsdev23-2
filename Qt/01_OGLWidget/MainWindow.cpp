#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->txSlider, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setTranslateX(int)));
    connect(ui->tySlider, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setTranslateY(int)));
    connect(ui->tzSlider, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setTranslateZ(int)));

    // connect(ui->sxSpinBox, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setScaleX(int)));
    // connect(ui->sySpinBox, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setScaleY(int)));
    // connect(ui->szSpinBox, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setScaleZ(int)));

    // connect(ui->rxSlider, SIGNAL(valueChanged(int)), ui->glWidget, SLOT(setXRotation(int)));

    // connect(ui->txSlider_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setTranslateX(int)));
    // connect(ui->tySlider_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setTranslateY(int)));
    // connect(ui->tzSlider_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setTranslateZ(int)));

    // connect(ui->sxSpinBox_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setScaleX(int)));
    // connect(ui->sySpinBox_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setScaleY(int)));
    // connect(ui->szSpinBox_2, SIGNAL(valueChanged(int)), ui->glWidget2, SLOT(setScaleZ(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
